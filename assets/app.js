/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)

// start the Stimulus application
// import "./bootstrap";

const $ = require("jquery");
global.$ = global.jQuery = $;
require("bootstrap");
// Using ESM specification
// import '/path/to/glightbox.js';

// Using a bundler like webpack
// import GLightbox from 'glightbox';

import "./vendor/bootstrap-icons/bootstrap-icons.css";
import "./vendor/bootstrap/css/bootstrap.min.css";
import "./vendor/glightbox/css/glightbox.min.css";
import "./css/main.css";
// import "./styles/app.scss";
import "./vendor/bootstrap/js/bootstrap.bundle.min.js";
// import "./vendor/glightbox/js/glightbox.min.js";
import "./js/main.js";

$(() => {
  $("a.ajax").on("click", (evtClick) => {
    evtClick.preventDefault();
    var href = evtClick.target.getAttribute("href");
    console.log(href);
    $.ajax({
      url: href,
      dataType: "json",
      success: (data) => {
        $("#nombre").html(data);
        console.log(data);
      },
      error: (jqXHR, status, error) => {
        console.log("ERREUR AJAX", status, error);
      },
    });
  });

  $("#formSearch").on("submit", (evtSubmit) => {
    evtSubmit.preventDefault();
    $.ajax({
      url: evtSubmit.target.getAttribute("action"),
      data: "search=" + $("#formSearch #search").val(),
      dataType: "html",
      success: (data) => {
        $("#main").html(data);
      },
      error: (jqXHR, status, error) => {
        console.log("ERREUR AJAX", status, error);
      },
    });
  });
});

// document.addEventListener("DOMContentLoaded", () => {
//   /**
//    * Initiate glightbox
//    */
//   const glightbox = GLightbox({
//     selector: ".glightbox",
//   });
// });
