<?php

namespace App\EventSubscriber;

use Twig\Environment;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class TwigEventSubscriber implements EventSubscriberInterface
{
    private $twig;
    private $session;

    public function __construct(Environment $twig, SessionInterface $session)
    {
        $this->twig = $twig;
        $this->session = $session;
    }
    public function onControllerEvent(ControllerEvent $event): void
    {
        $quantitePanier = 0;
        $panier = $this->session->get('panier', []);
        foreach($panier as $ligne) 
        {
            $quantitePanier += $ligne["quantite"];
        }
        $this->twig->addGlobal('quantitePanier', $quantitePanier);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ControllerEvent::class => 'onControllerEvent',
        ];
    }
}
