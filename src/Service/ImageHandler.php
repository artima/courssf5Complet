<?php

// src/Service/ImageHandler.php
namespace App\Service;

use Doctrine\ORM\Mapping\Entity;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImageHandler
{
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    public function mangePhoto($entity, $form, $entityRepository)
    {

        if ($fichier = $form->get("photo")->getData()) {
            $nomFichier = pathinfo($fichier->getClientOriginalName(), PATHINFO_FILENAME);
            $slugger = new AsciiSlugger();
            $nomFichier = $slugger->slug($nomFichier);
            $nomFichier .= "_" . uniqid();
			
            $nomFichier .= "." . $fichier->guessExtension();
            $fichier->move("images", $nomFichier);

            if ($entity->getPhoto()) {
                $fichier = $this->params->get("image_directory") . $entity->getPhoto();
                if (file_exists($fichier)) {
                    unlink($fichier);
                }
            }
            $entity->setPhoto($nomFichier);
        }
        $entityRepository->add($entity, true);
        
    }
}