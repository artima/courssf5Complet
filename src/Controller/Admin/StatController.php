<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StatController extends AbstractController
{
    # [Route('/stat', name: 'app_stat')]
    
    /**
    * @Route("/admin-statistique", name="app_admin_statistique_show")
    */

    public function show(): Response
    {
        return $this->render('admin/stat/stats.html.twig', [
            'controller_name' => 'StatController',
        ]);
    }

        /**
    * @Route("/admin-statistique", name="app_admin_commande_statistique")
    */

}
