<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PhotoFolioController extends AbstractController
{
    #[Route('/photo/folio', name: 'app_photo_folio')]
    public function index(): Response
    {
        return $this->render('photo_folio/index.html.twig', [
            'controller_name' => 'PhotoFolioController',
        ]);
    }
}
