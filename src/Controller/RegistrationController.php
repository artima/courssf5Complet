<?php

namespace App\Controller;

use App\Entity\Client;
use App\Form\RegistrationFormType;
use App\Security\LoginAuthenticator;
use App\Service\SendEmail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/inscription", name="app_register")
     */
    public function register(
        Request $request, 
        UserPasswordHasherInterface $userPasswordHasher, 
        UserAuthenticatorInterface $userAuthenticator, 
        LoginAuthenticator $authenticator, 
        EntityManagerInterface $entityManager,
        SendEmail $sendEmail): Response
    {
        $user = new Client();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $userPasswordHasher->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $user->setRoles(["ROLE_CLIENT"]);
            $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email

            $sendEmail->send([
                'recipient_email'   => $user->getEmail(),
                'subject'           => "Vérification de votre adresse Email",
                'html_template'     => "registration/register_confirmation_email.html.twig",
                'context'           => [
                    'userId' => $user->getId(),
                    'registartionToken' => "azert1234"
                ]
            ]);

            return $userAuthenticator->authenticateUser(
                $user,
                $authenticator,
                $request
            );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/{token}", name="app_verify_account", methods={"GET"})
     */
    // public function verifyUserAccount (EntityManagerInterface $em, Client $user, string $token): Response
    // {
    //     return $this
    // }

}
